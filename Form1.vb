﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Button1.Enabled = False
        Dim pwd As String = TextBox2.Text
        If CheckBox1.Checked Then
            If TextBox2.Text = "" Then
                MsgBox("请输入密码", 48 + 4096, "错误")
                Button2.Enabled = True
                Exit Sub
            End If
        Else
            pwd = ""
        End If
        If (TextBox1.Text.IndexOf("insert") > -1 And TextBox1.Text.IndexOf("into") > -1) Or (TextBox1.Text.IndexOf("update") > -1) Then
            gSQLite.gInsert(Label1.Text, pwd, TextBox1.Text)
        Else
            DataGridView1.DataSource = gSQLite.gDataTable(Label1.Text, pwd, TextBox1.Text)
            DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        End If
        Button1.Enabled = True
    End Sub
    Private Sub Label1_DragEnter(sender As Object, e As DragEventArgs) Handles Label1.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            Dim files As String()
            Try
                files = CType(e.Data.GetData(DataFormats.FileDrop), String())
                Label1.Text = files(0)
                Button1.Enabled = False
                Button2.Enabled = True
            Catch ex As Exception
                MsgBox(ex.Message, 48 + 4096, "错误")
                Return
            End Try
        End If
    End Sub
    Private Sub ListBox1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles ListBox1.MouseDoubleClick
        If ListBox1.SelectedIndex > -1 Then
            TextBox1.Text = "select * from " & ListBox1.Items.Item(ListBox1.SelectedIndex) & ";"
            Application.DoEvents()
            Button1_Click("自动查询", EventArgs.Empty)
        End If
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Button2.Enabled = False
        Dim pwd As String = TextBox2.Text
        If CheckBox1.Checked Then
            If TextBox2.Text = "" Then
                MsgBox("请输入密码", 48 + 4096, "错误")
                Button2.Enabled = True
                Exit Sub
            End If
        Else
            pwd = ""
        End If
        Dim sql As String = "select name from sqlite_master where type='table';"
        Dim dt As Data.DataTable = gSQLite.gDataTable(Label1.Text, pwd, sql)
        ListBox1.Items.Clear()
        For i = 0 To dt.Rows.Count - 1
            ListBox1.Items.Add(dt.Rows(i).Item(0).ToString)
        Next
        Button1.Enabled = True
        Button3.Enabled = True
        Button4.Enabled = True
    End Sub
    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked Then
            DataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Else
            DataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithAutoHeaderText
        End If
    End Sub
    Private Sub 导出数据ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles 导出数据ToolStripMenuItem.Click
        Dim ms As IO.MemoryStream = gOpenXml.gExcelOut("导出数据", CType(DataGridView1.DataSource, DataTable))
        Dim sfd As New SaveFileDialog
        sfd.Title = "导出数据"
        sfd.Filter = "Excel|*.xlsx"
        sfd.ShowDialog()
        If sfd.FileName <> "" Then
            IO.File.WriteAllBytes(sfd.FileName, ms.ToArray)
        End If
    End Sub
    Private Sub 打开ExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles 打开ExcelToolStripMenuItem.Click
        Dim ofd As New OpenFileDialog
        ofd.Title = "打开Excel"
        ofd.Filter = "Excel|*.xlsx"
        ofd.ShowDialog()
        If IO.File.Exists(ofd.FileName) Then
            DataGridView1.DataSource = gOpenXml.gExcelRead(New IO.MemoryStream(IO.File.ReadAllBytes(ofd.FileName))).Tables(0)
        End If
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Button3.Enabled = False
        Dim pwd As String = TextBox2.Text
        If CheckBox1.Checked Then
            If TextBox2.Text = "" Then
                MsgBox("请输入密码", 48 + 4096, "错误")
                Button3.Enabled = True
                Exit Sub
            End If
        Else
            pwd = ""
        End If
        CheckBox1.Checked = False
        gSQLite.ClearPwd(Label1.Text, pwd)
        Button3.Enabled = True
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Button4.Enabled = False
        If TextBox2.Text = "" Then
            MsgBox("请输入密码", 48 + 4096, "错误")
            Button4.Enabled = True
            Exit Sub
        End If
        gSQLite.AddPwd(Label1.Text, TextBox2.Text)
        Button4.Enabled = True
    End Sub
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim libname = IO.Path.Combine(IO.Directory.GetCurrentDirectory, "sqlite3.dll")
        If IO.File.Exists(libname) Then
            IO.File.Delete(libname)
        End If
        If IntPtr.Size = 4 Then
            IO.File.Copy(IO.Path.Combine(IO.Directory.GetCurrentDirectory, "sqlite3_x86.dll"), libname)
        Else
            IO.File.Copy(IO.Path.Combine(IO.Directory.GetCurrentDirectory, "sqlite3_x64.dll"), libname)
        End If
    End Sub
End Class