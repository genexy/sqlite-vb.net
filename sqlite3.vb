﻿Public Class sqlite3
    Public Const SQLITE_OK As Integer = 0 '成功
    Public Const SQLITE_ERROR As Integer = 1 'SQL错误或缺少数据库
    Public Const SQLITE_INTERNAL As Integer = 2 'SQLite中的内部逻辑错误
    Public Const SQLITE_PERM As Integer = 3 '访问权限被拒绝
    Public Const SQLITE_ABORT As Integer = 4 '回调例程请求中止
    Public Const SQLITE_BUSY As Integer = 5 '数据库文件已锁定
    Public Const SQLITE_LOCKED As Integer = 6 '数据库中的一个表被锁定
    Public Const SQLITE_NOMEM As Integer = 7 '一个malloc()失败
    Public Const SQLITE_READONLY As Integer = 8 '尝试写入只读数据库
    Public Const SQLITE_INTERRUPT As Integer = 9 '由sqlite3_interrupt()终止的操作
    Public Const SQLITE_IOERR As Integer = 10 '发生某种磁盘I/O错误
    Public Const SQLITE_CORRUPT As Integer = 11 '数据库磁盘映像格式不正确
    Public Const SQLITE_NOTFOUND As Integer = 12 '未知操作码sqlite3_interrupt()
    Public Const SQLITE_FULL As Integer = 13 '插入失败，因为数据库已满
    Public Const SQLITE_CANTOPEN As Integer = 14 '无法打开数据库文件
    Public Const SQLITE_PROTOCOL As Integer = 15 '数据库锁协议错误
    Public Const SQLITE_EMPTY As Integer = 16 '数据库为空
    Public Const SQLITE_SCHEMA As Integer = 17 '数据库架构已更改
    Public Const SQLITE_TOOBIG As Integer = 18 '字符串或BLOB超出大小限制
    Public Const SQLITE_CONSTRAINT As Integer = 19 '由于约束冲突而中止
    Public Const SQLITE_MISMATCH As Integer = 20 '数据类型不匹配
    Public Const SQLITE_MISUSE As Integer = 21 '库使用错误
    Public Const SQLITE_NOLFS As Integer = 22 '使用主机不支持的操作系统功能
    Public Const SQLITE_AUTH As Integer = 23 '授权被拒绝
    Public Const SQLITE_FORMAT As Integer = 24 '辅助数据库格式错误
    Public Const SQLITE_RANGE As Integer = 25 '第二个参数定义为sqlite3_bind超出范围
    Public Const SQLITE_NOTADB As Integer = 26 '文件被加密或不是有效的数据库
    Public Const SQLITE_NOTICE As Integer = 27 '来自sqlite3_log()的通知
    Public Const SQLITE_WARNING As Integer = 28 '来自sqlite3_log()的警告
    Public Const SQLITE_ROW As Integer = 100 'sqlite3_step()已准备好另一行
    Public Const SQLITE_DONE As Integer = 101 'sqlite3_step()已完成执行

    Public Const SQLITE_INTEGER As Integer = 1 '整数
    Public Const SQLITE_FLOAT As Integer = 2 '浮点
    Public Const SQLITE_TEXT As Integer = 3 '文本
    Public Const SQLITE_BLOB As Integer = 4 '二进制数据
    Public Const SQLITE_NULL As Integer = 5 '空值

    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_open(ByVal filename() As Byte, ByRef pdb As IntPtr) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True, CharSet:=Runtime.InteropServices.CharSet.Unicode)>
    Public Shared Function sqlite3_open16(ByVal filename As String, ByRef pdb As IntPtr) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_key(ByVal pdb As IntPtr, ByVal pkey As String, ByVal nkey As Integer) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_rekey(ByVal pdb As IntPtr, ByVal pkey As String, ByVal nkey As Integer) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_prepare_v2(ByVal pdb As IntPtr, ByVal sql() As Byte, ByVal nByte As Integer, ByRef ppStmpt As IntPtr, ByVal pzTail As IntPtr) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_step(ByVal stmHandle As IntPtr) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_finalize(ByVal stmHandle As IntPtr) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_column_origin_name(ByVal stmHandle As IntPtr, ByVal iCol As Integer) As IntPtr
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True, CharSet:=Runtime.InteropServices.CharSet.Unicode)>
    Public Shared Function sqlite3_column_origin_name16(ByVal stmHandle As IntPtr, ByVal iCol As Integer) As IntPtr
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_column_int(ByVal stmHandle As IntPtr, ByVal iCol As Integer) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_column_type(ByVal stmHandle As IntPtr, ByVal iCol As Integer) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True, CharSet:=Runtime.InteropServices.CharSet.Unicode)>
    Public Shared Function sqlite3_column_text16(ByVal stmHandle As IntPtr, ByVal iCol As Integer) As IntPtr
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_column_text(ByVal stmHandle As IntPtr, ByVal iCol As Integer) As IntPtr
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_column_double(ByVal stmHandle As IntPtr, ByVal iCol As Integer) As Double
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_column_blob(ByVal stmHandle As IntPtr, ByVal iCol As Integer) As Byte()
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_errmsg(ByVal pdb As IntPtr) As String
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_get_table(ByVal pdb As IntPtr, ByVal sql() As Byte, ByRef pazResult As String(), ByRef pnRow As IntPtr, ByVal pnColumn As IntPtr, ByRef pzErrmsg As String()) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_column_count(ByVal stmHandle As IntPtr) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_exec(ByVal pdb As IntPtr, ByVal sql() As Byte, ByVal sqlite_callback As Integer, ByVal void As Integer, ByVal chr As Integer) As Integer
    End Function
    <Runtime.InteropServices.DllImport("sqlite3.dll", SetLastError:=True)>
    Public Shared Function sqlite3_close(ByVal pdb As IntPtr) As Integer
    End Function
End Class